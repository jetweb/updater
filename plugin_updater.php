<?php

/*
 * 
 *  Jet_Update File for plugins:
 * Usage:
 *  1. Include this file in functions.php
 *  2. When upload the plugin to update.jetweb.co.il the slug name must be the name of the plugin file.
 * 
 * Version: 1.0.0
 * Author Jetweb
 */
if (!class_exists('Jet_Update')):
    class Jet_Update {

        private $version;
        private $api_url, $plugin_slug;

        function __construct($api_url = 'http://updates.jetweb.co.il') {
            $this->version = "1.0.0";
            $backtrace = debug_backtrace();
            $calling_file = $backtrace[0]['file'];
            $plugin = basename($calling_file, ".php");

            $this->plugin_slug = $plugin;
            $this->api_url = $api_url;
            add_filter('pre_set_site_transient_update_plugins', array($this, 'check_for_plugin_update'));
            add_filter('plugins_api', array($this, 'plugin_api_call'), 10, 3);
        }

        public function check_for_plugin_update($checked_data) {
            global $wp_version;

            if (empty($checked_data->checked)) {
                return $checked_data;
            }

            $args = [
                'slug' => $this->plugin_slug,
                'version' => $checked_data->checked[$this->plugin_slug . '/' . $this->plugin_slug . '.php']
            ];

            $request_string = [
                'body' => [
                    'action' => 'basic_check',
                    'request' => serialize($args),
                    'api-key' => md5(get_bloginfo('url'))
                ],
                'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
            ];

            // start checking for an update
            $raw_response = wp_remote_post($this->api_url, $request_string);
            if (!is_wp_error($raw_response) && ($raw_response['response']['code'] == 200)) {
                $response = unserialize($raw_response['body']);
            }

            if (isset($response) && is_object($response) && !empty($response)) {
                $checked_data->response[$this->plugin_slug . '/' . $this->plugin_slug . '.php'] = $response;
            }
            return $checked_data;
        }

        public function plugin_api_call($def, $action, $args) {
            global $wp_version;

            if (!isset($args->slug) || ($args->slug != $this->plugin_slug)) {
                return false;
            }

            // get the current version
            $plugin_info = get_site_transient('update_plugins');
            $current_version = $plugin_info->checked[$this->plugin_slug . '/' . $this->plugin_slug . '.php'];
            $args->version = $current_version;

            $request_string = [
                'body' => [
                    'action' => $action,
                    'request' => serialize($args),
                    'api-key' => md5(get_bloginfo('url'))
                ],
                'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
            ];

            $request = wp_remote_post($this->api_url, $request_string);

            if (is_wp_error($request)) {
                $res = new WP_Error('plugins_api_failed', __('An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a>'), $request->get_error_message());
            } else {
                $res = unserialize($request['body']);

                if ($res === false) {
                    $res = new WP_Error('plugins_api_failed', __('An unknown error occurred'), $request['body']);
                }
            }
            return $res;
        }

    }
    
    
    
    
    
endif;